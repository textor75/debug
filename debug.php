<?php
/**
 * Подключаем
 * include_once __DIR__ . '/debug.php';
 *
 * Определяем DEV-окружение, по каким либо своим критериям
 * \x::dev(true)
 *
 * Некоторые функции для вывода информации для дебагинга
 *
 * pre(mixed arg1 [, arg2, ... argN]) - вывод аргументов функциуй print_r в html формате;
 * dv(mixed arg1 [, arg2, ... argN]) - то же, что и pre(), но после вывода делает die;
 *
 * prep(mixed arg1 [, arg2, ... argN]) - выводит аргументы в виде раскрывающегося дерева.
 *        Удобнее вышеперечисленных, но не работает при слишком большом объеме
 *        и не выводит не публичные свойства
 * dvp(mixed arg1 [, arg2, ... argN]) - отличается от prep(), как dv() от pre()
 *
 * apre(mixed arg1 [, arg2, ... argN])
 * adv(mixed arg1 [, arg2, ... argN]) - делают тоже, что pre() и dv(), но в простом текстовом формате.
 *
 * tr(bool withoutArguments) - выводит информацию из backtrace и делает die;
 *        withoutArguments - отказаться от вывода аргументов, по умолчанию - true (не выводить)
 *
 * mylog(mixed arg, string logType) - выводит arg в файл log_[logType].log в корне сайта
 *        logType - по умолчанию - base
 *
 */
$debug_time = microtime(TRUE);

/*
function dvp()
{
    $array = func_get_args();
    _prep($array);
    die;
}

function prep()
{
    $array = func_get_args();
    _prep($array);
}

function prepn()
{
    $array = func_get_args();
    _prep($array, false);
}
*/

function _prep($array, $sort = true)
{
    $aTrack = debug_backtrace();
    $aTrack = $aTrack[1];
    $sInfo = $aTrack['file'] . ':' . $aTrack['line'];
    $sClass = md5($sInfo);
    $aArgs = _args(2);
    echo '<div  id="dvp_' . $sClass . '" style="text-align: left; margin: 0 0 15px; color: #000; padding: 10px; border: dashed 1px #AAC; background: #F8F8F8; ">';
    echo '<div style="font: 8pt Arial; color: #888; margin-left: 30px; float: right;">' . $sInfo . '</div>';
    foreach ($array as $i => $mixed) {
        echo '<div>' . (isset($aArgs[$i]) ? $aArgs[$i] : '&laquo;' . ($i + 1) . '&raquo;') . ' = ';
        _dvp($mixed, $sort);
        echo '</div>';
    }
    echo '</div>';
}

function _dvp($var, $sort = true)
{
    switch (gettype($var)) {
        case 'string':
            echo '<span style="color: #5e2da0">«<i>' . htmlspecialchars($var) . '</i>»</span>';
            if (is_callable($var)) {
                echo ' <i>or</i> <span style="color: #0A0">function</span>';
            }
            break;
        case 'integer':
        case 'double':
            echo '<span style="color: #1b851b"><b>' . $var . '</b></span>';
            break;
        case 'boolean':
            echo '<span style="color: #' . ($var ? 'b0716a">TRUE' : '6aa7b0">FALSE') . '</span>';
            break;
        case 'array':
        case 'object':
            $id = md5(mt_rand(0, 9999) . microtime());

            $count = count($var);
            echo gettype($var);
            if (gettype($var) == 'object') {
                $aVars = get_object_vars($var);
                $count = count($aVars);
                if (is_callable($var)) {
                    echo ' <i>or</i> <span style="color: #0A0">function</span>';
                }
                echo ' (' . get_class($var) . ')';
                $var = $aVars;
            }
            echo ' (' . $count . ')';

            if (!$count) {
                break;
            }
            echo ' <span class="opn" style="cursor: pointer; color: #7D7;" onClick="bIsClose=document.getElementById(' . "'dvp" . $id . "'" . ').style.display == \'none\'; document.getElementById(' . "'dvp" . $id . "'" . ').style.display = bIsClose ? \'block\' : \'none\'; this.style.color = bIsClose ? \'#161\' : \'#7D7\';">&#9660;</span>';
            echo '<div style="margin-left: 30px; border-left: solid 2px #DDF; display: none;" id="dvp' . $id . '">';
            if ($sort) ksort($var);
            foreach ($var as $i => $v) {
                echo '<div>' . $i . ' = ';
                _dvp($v, $sort);
                echo '</div>';
            }
            echo '</div>';
            break;
        case 'NULL':
            echo '<span style="color: #AAA">NULL</span>';
            break;
        default:
            echo (string)$var;
    }
}

function dv()
{
    $array = func_get_args();
    _dv($array);
    die;
}

function pre()
{
    $array = func_get_args();
    _dv($array);
}

function _args($h = 2)
{
    $aTrack = debug_backtrace();
    $aTrack = $aTrack[$h];
    $sLine = explode("\n", file_get_contents($aTrack['file']));
    $sLine = implode(' ', array_slice($sLine, $aTrack['line'] - 1));
    $match = preg_match('/' . $aTrack['function'] . '\s*\((.*)\)/ism', $sLine, $sArgs);
    $sArgs = $sArgs[1];
    $aArgs = array('');
    $i = 0;
    $l = strlen($sArgs);
    $iBrackets = 0;
    $pos = 0;
    while ($pos < $l) {
        $a = $sArgs[$pos];
        if ($a == ',' && !$iBrackets) {
            $i++;
            $Args[$i] = '';
            $pos++;
            continue;
        } elseif ($a == ')') {
            if (!$iBrackets) {
                break;
            }
            $iBrackets--;
        } elseif ($a == '(') {
            $iBrackets++;
        } elseif ($a == "'" || $a == '"') {
            $pos2 = $pos;
            while (true) {
                $pos2 = strpos($sArgs, $a, $pos2 + 1);
                if ($pos2 === false) {
                    break 2;
                } else {
                    if ($sArgs[$pos2 - 1] != '\\') {
                        break;
                    }
                }
            }

            $a = substr($sArgs, $pos, $pos2 - $pos + 1);
            $pos = $pos2;
        }
        if (!isset($aArgs[$i])) {
            $aArgs[$i] = '';
        }
        $aArgs[$i] .= $a;
        $pos++;
    }
    return array_map('trim', $aArgs);
}

function _dv($array)
{
    $aArgs = _args();
    $aTrack = debug_backtrace();
    $aTrack = $aTrack[1];
    $sInfo = $aTrack['file'] . ':' . $aTrack['line'];
    foreach ($array as $i => $mixed) {
        echo '<pre style="text-align: left;margin: 0 0 15px; color: #000; padding: 10px; border: dashed 1px #AAC; background: linear-gradient(to bottom, #FFF, #CCC); overflow-x: auto;">' .
            '<div style="border-bottom: solid 1px #AAA; color: #66C; padding-bottom: 10px; margin-bottom: 10px;">' .
            '<span style="font: 8pt Arial; color: #888; float: right; margin-left: 30px;">' . $sInfo . '</span>' .
            (isset($aArgs[$i]) ? $aArgs[$i] : '&laquo;' . ($i + 1) . '&raquo;') .
            '</div>' .
            htmlspecialchars(print_r($mixed, 1)) .
            '</pre>';
    }
}

function tr($bWithoutArgs = true)
{
    $tracker = debug_backtrace();
    if ($bWithoutArgs) {
        $tracker = array_map(
            function ($a) {
                unset($a['args']);
                return $a;
            },
            $tracker
        );
    }
    dv($tracker);
}

function adv($mixed)
{
    $array = func_get_args();
    _adv($array);
    die;
}

function apre()
{
    $array = func_get_args();
    _adv($array);
    echo '_______________' . PHP_EOL;
}

function _adv($array)
{
    $aArgs = _args();
    foreach ($array as $i => $mixed) {
        echo PHP_EOL . '<' . (isset($aArgs[$i]) ? $aArgs[$i] : $i + 1) . '>' . PHP_EOL . print_r($mixed, 1) . PHP_EOL;
    }
}

function mylog($message, $file_name = 'base', $trackBack = 0)
{
    $tracker = debug_backtrace();
    $tracker = $tracker[$trackBack];
    $file = basename($tracker['file']) . ' : ' . $tracker['line'];
    $text = array(
        date('d.m.Y H:i:s, ') . $file,
        '-------------------------------',
        print_r($message, 1),
        '===============================',
        '',
        ''
    );
    $f = fopen(__DIR__ . '/log_' . $file_name . '.log', 'a+');
    $time = date('d.m.Y H:i:s, ') . $file . PHP_EOL;

    fwrite($f, implode(PHP_EOL, $text));
    fclose($f);

}

function mytime()
{
}

class x
{
    const FILE = 1;
    const SHORT = 2;
    const LINE = 4;
    const VARS = 8;
    const TIME = 16;
    const MEMO = 32;
    const STOP = 64;
    const ALL = 127;

    const _DEFAULT = 'def';
    const _SHOW = 'show';
    const _TREE = 'list';
    const _TEXT = 'text';
    const _LOG = 'log';

    const _LOGDIR = 'logdir';
    const _LOGFILE = 'logfile';
    const _LOGEXT = 'logext';
    const _TREESORT = 'treesort';


    private static $fTime;
    private static $aConfig = [
        self::_DEFAULT => self::FILE | self::LINE | self::VARS,
        self::_LOGDIR => __DIR__,
        self::_LOGFILE => 'base',
        self::_LOGEXT => 'log',
        self::_TREESORT => TRUE,
    ];
    private static $aConfigTmp = [];
    private static $bInit = FALSE;
    private static $bDev = FALSE;
    private static $aTmpTreeObjects = [];

    // *** Public methods ***

    public static function dev($isDev = FALSE)
    {
        self::$bDev = (bool)$isDev;
        self::init();
    }

    public static function isDev()
    {
        return self::$bDev;
    }

    public static function stop()
    {
        if (!self::$bDev) {
            return;
        }
        die;
    }

    public static function show()
    {
        if (!self::$bDev) {
            return;
        }
        self::perform_show(func_get_args());
    }

    public static function tree()
    {
        if (!self::$bDev) {
            return;
        }
        self::perform_tree(func_get_args());
    }

    public static function log()
    {
        if (!self::$bDev) {
            return;
        }
        $args = func_get_args();
        mylog(count($args) > 1 ? $args : reset($args), 'base', 1);
        //self::perform_log(func_get_args());
    }

    // *** LOG ***

    private static function perform_log($aArgs)
    {

    }

    // *** TIME ***

    private static function getTime($sName = '', $mDiff = 0)
    {
        if (!self::$bDev) {
            return;
        }

        return round(microtime(TRUE) - self::$fTime,3);
    }

    // *** TREE ***

    private static $sTreeIdPrefix = 'x_tree_';
    private static $sTreeContainer = 'div';
    private static $aTreeStyle = [
        'text-align' => 'left',
        'margin' => '0 0 15px',
        'color' => '#000',
        'padding' => '10px',
        'border' => 'dashed 1px #AAC',
        'background' => '#F8F8F8',
    ];
    private static $sTreeHeadrContainer = 'div';
    private static $aTreeHeadrStyle = [
        'font' => '8pt Arial',
        'color' => '#888',
        'margin-left' => '30px',
        'float' => 'right',
    ];
    private static $sTreeArgContainer = 'div';
    private static $sTreeTypeContainer = 'span';
    private static $aTreeTypeStringStyle = ['color' => '#5e2da0', 'font-style' => 'italic'];
    private static $aTreeTypeNumberStyle = ['color' => '#1b851b', 'font-weight' => 'bold'];
    private static $aTreeTypeFunctionStyle = ['color' => '#0A0'];
    private static $aTreeTrueStyle = ['color' => '#b0716a'];
    private static $aTreeFalseStyle = ['color' => '#6aa7b0'];
    private static $aTreeNullStyle = ['color' => '#AAA'];
    private static $sTreeControlContainer = 'span';
    private static $aTreeControlStyle = [
        'cursor' => 'pointer',
        'color' => '#7D7',
    ];
    private static $aTreeControlCloseColor = '#161';
    private static $aTreeControlOpenColor = '#7D7';
    private static $sTreeValueContainer = 'div';
    private static $aTreeValueStyle = [
        'margin-left' => '30px',
        'border-left' => 'solid 2px #DDF',
        'display' => 'none',
    ];

    private static function perform_tree($aArgs)
    {
        $iParams = self::get_params(self::_TREE);
        $aStatistic = self::get_statistic($iParams);
        $aInfo = self::get_info($aStatistic);
        $sInfo = implode(', ', $aInfo);
        $sId = self::$sTreeIdPrefix . md5('info: ' . $sInfo);
        self::open(self::$sTreeContainer, self::$aTreeStyle, ['id' => $sId]);
        if ($sInfo) {
            self::open(self::$sTreeHeadrContainer, self::$aTreeHeadrStyle);
            echo htmlspecialchars(implode(', ', $aInfo));
            self::close(self::$sTreeHeadrContainer);
        }
        foreach ($aArgs as $i => $mValue) {
            self::open(self::$sTreeArgContainer);
            $sArg = self::get_var($aStatistic, $i, TRUE);
            echo $sArg . ' = ';
            self::perform_tree_item($mValue, array($sArg));
            self::close(self::$sTreeArgContainer);
            self::$aTmpTreeObjects = array();
        }
        self::close(self::$sTreeContainer);

    }

    private static function perform_tree_item(&$mValue, $aPath = [])
    {
        switch (gettype($mValue)) {
            case 'string':
                self::open(self::$sTreeTypeContainer, self::$aTreeTypeStringStyle);
                echo '«' . htmlspecialchars($mValue) . '»';
                self::close(self::$sTreeTypeContainer);
                if (is_callable($mValue)) {
                    echo ' or ';
                    self::open(self::$sTreeTypeContainer, self::$aTreeTypeFunctionStyle);
                    echo 'function';
                    self::close(self::$sTreeTypeContainer);
                }
                break;
            case 'integer':
            case 'double':
                self::open(self::$sTreeTypeContainer, self::$aTreeTypeNumberStyle);
                echo $mValue;
                self::close(self::$sTreeTypeContainer);
                break;
            case 'boolean':
                self::open(self::$sTreeTypeContainer, $mValue ? self::$aTreeTrueStyle : self::$aTreeFalseStyle);
                echo $mValue ? 'TRUE' : 'FALSE';
                self::close(self::$sTreeTypeContainer);
                break;
            case 'array':
            case 'object':
                $sRecursion = '';
                foreach (self::$aTmpTreeObjects as $sId => &$aObjectParams) {
                    if (self::objects_is_ref($mValue, $aObjectParams['object'])) {
                        $sRecursion = 'Reference to element as ' . implode(' &#8594; ', $aObjectParams['path']); // TODO as link
                        break;
                    } elseif (is_array($mValue) && !empty($mValue) && $mValue === $aObjectParams['object']) {
                        $sRecursion = 'Same array as ' . implode(' &#8594; ', $aObjectParams['path']); // TODO as link
                        break;
                    }
                }

                if ($sRecursion) {
                    self::open(self::$sTreeTypeContainer, self::$aTreeTypeFunctionStyle);
                    echo $sRecursion;
                    self::close(self::$sTreeTypeContainer);
                    break;
                }

                $sId = uniqid(self::$sTreeIdPrefix, TRUE);

                self::$aTmpTreeObjects[$sId] = array(
                    'object' => &$mValue,
                    'path' => $aPath,
                );

                $iCount = count($mValue);
                echo gettype($mValue);
                if (gettype($mValue) === 'object') {
                    $aVars = get_object_vars($mValue);
                    $iCount = count($aVars);
                    if (is_callable($mValue)) {
                        echo ' or ';
                        self::open(self::$sTreeTypeContainer, self::$aTreeTypeFunctionStyle);
                        echo 'function';
                        self::close(self::$sTreeTypeContainer);
                    }
                    echo ' (' . get_class($mValue) . ')';
                    $aKeys = array_keys($aVars);
                } else {
                    $aKeys = array_keys($mValue);
                }
                if (self::get_params(self::_TREESORT)) {
                    sort($aKeys);
                }
                echo ' (' . $iCount . ')';

                if (!$iCount) {
                    break;
                }

                $sScript = 'bIsClose=document.getElementById(\'' . $sId . '\').style.display == \'none\';'
                    . 'document.getElementById(\'' . $sId . '\').style.display = bIsClose ? \'block\' : \'none\';'
                    . 'this.style.color = bIsClose ? \'' . self::$aTreeControlCloseColor . '\' : \'' . self::$aTreeControlOpenColor . '\';';

                self::open(
                    self::$sTreeControlContainer,
                    self::$aTreeControlStyle,
                    array('onClick' => $sScript)
                );
                echo '&#9660;';
                self::close(self::$sTreeControlContainer);
                self::open(
                    self::$sTreeValueContainer,
                    self::$aTreeValueStyle,
                    array('id' => $sId)
                );
                // if ($sort) ksort($var); TODO sort as param
                foreach ($aKeys as $mKey) {
                    if (gettype($mValue) === 'object') {
                        $mVal = &$mValue->$mKey;
                    } else {
                        $mVal = &$mValue[$mKey];
                    }
                    self::open(self::$sTreeArgContainer);
                    echo $mKey . ' = ';
                    self::perform_tree_item($mVal, array_merge($aPath, array($mKey)));
                    self::close(self::$sTreeArgContainer);
                }
                self::close(self::$sTreeValueContainer);
                break;
            case 'NULL':
                self::open(self::$sTreeTypeContainer, self::$aTreeNullStyle);
                echo 'NULL';
                self::close(self::$sTreeTypeContainer);
                break;
            // TODO for is_resource()
            default:
                echo (string)$mValue;
        }
    }

    // *** SHOW ***

    private static $sShowContainer = 'pre';
    private static$aShowStyle = [
        'font' => '11pt Arial',
        'text-align' => 'left',
        'margin' => '0 0 15px',
        'color' => '#000',
        'padding' => '10px',
        'border' => 'dashed 1px #AAC',
        'background' => 'linear-gradient(to bottom, #FFF, #CCC)',
        'overflow-x' => 'auto',
        'white-space' => 'pre-wrap',
    ];
    private static $sShowHeaderContainer = 'div';
    private static $aShowHeaderStyle = [
        'border-bottom' => 'solid 1px #AAA',
        'color' => '#66C',
        'padding-bottom' => '10px',
        'margin-bottom' => '10px',
    ];
    private static $sShowInfoContainer = 'span';
    private static $aShowInfoStyle = [
        'font' => '8pt Arial',
        'color' => '#888',
        'float' => 'right',
        'margin-left' => '30px',
    ];

    private static function perform_show($aArgs)
    {
        $iParams = self::get_params(self::_SHOW);
        $aStatistic = self::get_statistic($iParams);
        $aInfo = self::get_info($aStatistic);
        foreach ($aArgs as $i => $mValue) {
            self::open(self::$sShowContainer, self::$aShowStyle);
            if (!empty($aInfo) || isset($aStatistic[self::VARS])) {
                self::open(self::$sShowHeaderContainer, self::$aShowHeaderStyle);
                if (!empty($aInfo)) {
                    self::open(self::$sShowInfoContainer, self::$aShowInfoStyle);
                    echo htmlspecialchars(implode(', ', $aInfo));
                    self::close(self::$sShowInfoContainer);
                }
                self::varp($aStatistic, $i, TRUE);
                self::close(self::$sShowHeaderContainer);
            }
            echo htmlspecialchars(print_r($mValue, 1));
            self::close(self::$sShowContainer);
        }
        if ($iParams & self::STOP) {
            die();
        }
    }

    // *** Common ***

    private static function objects_is_ref(&$mObject1, &$mObject2)
    {
        if (gettype($mObject1) !== gettype($mObject2)) {
            return FALSE;
        }
        $bIsRef = FALSE;

        if (is_array($mObject1)) {
            do {
                $sKey = uniqid('is_ref_key_', TRUE);
            } while (isset($mObject1[$sKey]));
            $sValue = uniqid('is_ref_val_', TRUE);
            $mObject1[$sKey] = $sValue;

            if (isset($mObject2[$sKey]) && $mObject2[$sKey] === $sValue) {
                $mObject1[$sKey] .= 'x';
                if ($mObject2[$sKey] === $sValue . 'x') {
                    $bIsRef = TRUE;
                }
            }
            unset($mObject1[$sKey]);
        } elseif (is_object($mObject1)) {
            $bIsRef = $mObject1 === $mObject2;
        }

        return $bIsRef;
    }

    private static function varp($aStatistic, $i, $bIndexAsDefault = FALSE)
    {
        echo self::get_var($aStatistic, $i, $bIndexAsDefault);
    }

    /**
     * @param array $aStatistic
     * @param int $i
     * @param bool $bIndexAsDefault
     * @return string
     */
    private static function get_var($aStatistic, $i, $bIndexAsDefault = FALSE)
    {
        $result = '';

        if (isset($aStatistic[self::VARS][$i])) {
            $result = htmlspecialchars($aStatistic[self::VARS][$i]);
        } elseif ($bIndexAsDefault) {
            $result = '&laquo;' . ($i + 1) . '&raquo;';
        }

        return $result;
    }

    /**
     * @param string $sTag
     * @param array $aStyle
     * @param array $aAttr
     */
    private static function open($sTag, $aStyle = [], $aAttr = [])
    {
        $sOpen = '<' . $sTag;

        if (!empty($aStyle)) {
            $aTmp = array();
            foreach ($aStyle as $sProperty => $sValue) {
                $aTmp[] = str_replace('"', '&quot;', $sProperty . ':' . $sValue);
            }
            $sOpen .= ' style="' . implode(';', $aTmp) . '"';
        }
        if (!empty($aAttr)) {
            foreach ($aAttr as $sAttr => $sValue) {
                $sOpen .= ' ' . $sAttr . '="' . str_replace('"', '&quot;', $sValue) . '"';
            }
        }
        $sOpen .= '>';

        echo $sOpen;
    }

    /**
     * @param string $sTag
     */
    private static function close($sTag)
    {
        echo '</' . $sTag . '>';
    }

    /**
     * @param array $aStatistic
     * @return array
     */
    private static function get_info($aStatistic)
    {
        $sLine = '';
        if (isset($aStatistic[self::FILE])) {
            $sLine = 'file: ' . $aStatistic[self::FILE];
        }
        if (isset($aStatistic[self::LINE])) {
            $sLine .= ($sLine ? ':' : 'line: ') . $aStatistic[self::LINE];
        }
        $aInfo = array();
        if ($sLine) {
            $aInfo['line'] = $sLine;
        }
        if (isset($aStatistic[self::TIME])) {
            $aInfo['time'] = 'time: ' . $aStatistic[self::TIME];
        }
        if (isset($aStatistic[self::MEMO])) {
            $aInfo['memo'] = 'memory: ' . $aStatistic[self::MEMO];
        }
        return $aInfo;
    }

    private static function get_statistic($iParams, $iStek = 2)
    {
        $aStatistic = array();
        if ($iParams & self::MEMO) {
            $aStatistic[self::MEMO] = memory_get_usage(); // TODO
        }
        if ($iParams & self::TIME) {
            $aStatistic[self::TIME] = self::getTime(); // TODO
        }
        if ($iParams & (self::FILE | self::LINE | self::VARS)) {
            $aTracks = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            if (!isset($aTracks[$iStek])) {
                self::error('Unknown backtrace index: ' . $iStek . '.');
            }
            $aTrack = $aTracks[$iStek];
            if ($iParams & self::FILE) {
                $aStatistic[self::FILE] = $aTrack['file'];
            }
            if ($iParams & self::LINE) {
                $aStatistic[self::LINE] = $aTrack['line'];
            }
            if ($iParams & self::VARS) {
                $aStatistic[self::VARS] = self::get_args($aTrack);
            }
        }
        return $aStatistic;
    }

    private static function get_args($aTrack)
    {
        $sFile = $aTrack['file'];
        $iLine = $aTrack['line'];
        $aArgs = array();
        if (!is_readable($sFile)) {
            self::error('File ' . $sFile . ' is not readable.');
            return array();
        }
        $sCode = explode("\n", file_get_contents($sFile));
        $sCode = implode("\n", array_slice($sCode, 0, $iLine));
        $sCall = ($aTrack['type'] ? $aTrack['class'] . $aTrack['type'] : '') . $aTrack['function'] . '(';
        $iPos = mb_strrpos($sCode, $sCall);
        if ($iPos === FALSE) {
            return array();
        }
        $iPos += mb_strlen($sCall);
        $sLine = mb_substr($sCode, $iPos);
        $iLen = mb_strlen($sLine);
        $sArg = '';
        $aModes = array();
        $sMode = '';
        for ($i = 0; $i < $iLen; $i++) {
            $sChar = mb_substr($sLine, $i, 1);
            if ($sChar === ',' && !$sMode) {
                $aArgs[] = trim($sArg);
                $sArg = '';
                continue;
            }
            if ($sChar === ')' && !$sMode) {
                break;
            }
            $sArg .= $sChar;
            if ($sMode === '"' || $sMode === '\'') {
                if ($sChar === '\\') {
                    $sArg .= mb_substr($sLine, $i + 1, 1);
                    $i++;
                } elseif ($sChar === $sMode) {
                    $sMode = array_pop($aModes);
                }
            } elseif (in_array($sChar, array('"', '\'', '{', '[', '('))) {
                $aModes[] = $sMode;
                $sMode = $sChar;
            } elseif (
                ($sMode === '{' && $sChar === '}')
                ||
                ($sMode === '[' && $sChar === ']')
                ||
                ($sMode === '(' && $sChar === ')')
            ) {
                $sMode = array_pop($aModes);
            }
        }
        $aArgs[] = trim($sArg);
        return $aArgs;
    }

    private static function get_params_array($sFunc)
    {
        $iMask = self::get_params($sFunc);
        $aParams = array();
        foreach (self::$templateKeys as $int => $label) {
            if ($iMask & $int) {
                $aParams[$int] = $label;
            }
        }
        return $aParams;
    }

    private static function get_params($sFunc)
    {
        $iMask = self::$aConfig[self::_DEFAULT];

        if (isset(self::$aConfigTmp[$sFunc])) {
            $iMask = self::$aConfigTmp[$sFunc];
            unset(self::$aConfigTmp[$sFunc]);
        } elseif (isset(self::$aConfig[$sFunc])) {
            $iMask = self::$aConfig[$sFunc];
        }
        return $iMask;
    }

    private static function set_params($sFunc, $iMask = 0, $bTmp = FALSE)
    {
        if ($bTmp) {
            self::$aConfigTmp[$sFunc] = (int)$iMask;
        } else {
            self::$aConfig[$sFunc] = (int)$iMask;
        }
    }

    private static function init()
    {
        if (self::$bInit) {
            return;
        }
        self::$bInit = TRUE;
        self::$fTime = microtime(TRUE);
    }

    private static function error($sMessage = '')
    {
        $sMessage = 'DEBUG ERROR: ' . $sMessage;
        self::set_params(self::_SHOW, 0, TRUE);
        self::show($sMessage);
    }

}